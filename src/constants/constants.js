module.exports.REPLY = Object.freeze({
        SELECT_SEARCH_TYPE_MSG: 'Select search type',
        INCORRECT_INPUT: 'Your input is incorrect, enter again, please',
        ANY_BOOK_WRITTEN_IN: 'Sorry, I didn`t find any book written in ',
        ANY_BOOK_WRITTEN_BY: 'Sorry, I didn`t find any book written by ',
        FIND_BY_YEAR: "Find by year",
        FIND_BY_AUTHOR: "Find by author",
        ENTER_YEAR: "Enter year the book was written",
        ENTER_AUTHOR: "Enter author`s name",
        INVALID_YEAR: 'This year hasn`t come yet'
    }
);

module.exports.URL = Object.freeze({
        GET_STARTED_URL: 'https://graph.facebook.com/v2.6/me/messenger_profile?access_token=EAAL8mtJExvcBAGLtzmr53W6W5X09uCtEGEvEC9tXkWMyK3rbb5jtrSPQpbWXn8FcNW6s3NKYocxGuO3FyIxIDfgGxPPuHTpnAiDZBiNKMi6teNci3HIrZAPHAtWAyK5wuHvxo75TIs7IVf4yFOC1iec9OuOwnCZANEjGU0PBMwntdZC7ahYN',
        URI: 'https://graph.facebook.com/v3.3/me/messages?access_token=EAAL8mtJExvcBAGLtzmr53W6W5X09uCtEGEvEC9tXkWMyK3rbb5jtrSPQpbWXn8FcNW6s3NKYocxGuO3FyIxIDfgGxPPuHTpnAiDZBiNKMi6teNci3HIrZAPHAtWAyK5wuHvxo75TIs7IVf4yFOC1iec9OuOwnCZANEjGU0PBMwntdZC7ahYN',
        IMAGE_URI: "https://i.pinimg.com/736x/a0/67/5e/a0675e5161d7ae5be2550987f397a641--flower-shops-paper-flowers.jpg",
        PERSISTENT_MENU_URI: 'https://graph.facebook.com/v2.6/me/messenger_profile?access_token=EAAL8mtJExvcBAGLtzmr53W6W5X09uCtEGEvEC9tXkWMyK3rbb5jtrSPQpbWXn8FcNW6s3NKYocxGuO3FyIxIDfgGxPPuHTpnAiDZBiNKMi6teNci3HIrZAPHAtWAyK5wuHvxo75TIs7IVf4yFOC1iec9OuOwnCZANEjGU0PBMwntdZC7ahYN'
    }
);

module.exports.STATE = Object.freeze({
    START: 'start',
    DETAILS: 'details',
});


module.exports.PAYLOAD = Object.freeze({
    DETAILS: 'details',
    FIND: 'find',
    AUTHOR: 'author',
    YEAR: 'year',
    SEARCH: 'search'
});