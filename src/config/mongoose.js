var mongoose = require('mongoose');
const config  = require('./config');

mongoose.connect(config.MONGODB_URI, {useNewUrlParser: true});

var db = mongoose.connection;
db.on('error', console.error.bind(console, 'connection error:'));
db.once('open', ()=>{

   console.log(`Server started on ${config.PORT}`);

});
