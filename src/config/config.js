module.exports = {
   ENV: process.env.NODE_ENV || 'wer',
   PORT: 3000,
   URL: process.env.PORT ,
   MONGODB_URI: 'mongodb://127.0.0.1:27017/books'
};
