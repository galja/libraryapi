let messagingService = require('../service/messagingService');
let userService = require('../service/userService');
let bookService = require('../service/bookService');
let messaging = require('../models/message');
let constants = require('../constants/constants');

exports.verifyWebhook = (req, res) => {
    let verifyToken = "test";
    let token = req.query['hub.verify_token'];
    let challenge = req.query['hub.challenge'];

    if (token === verifyToken) {
        console.log('verified');
        res.status(200);
        res.end(challenge);
    } else {
        res.send(403);
    }
};

exports.processRequest = (req, res) => {
    let body = req.body;

    if (body.object === 'page') {
        let id = req.body.entry[0].messaging[0].sender.id;
        console.log(id);

        userService.findUser(id).then(user => {
            if (!user) {
                userService.insertUser(id, constants.STATE.START).then(doc => {
                    console.log(doc)
                }).catch(err => {
                    console.error(err)
                });
            }
            res.send(200);
            // console.log(user.state);
            chooseAction(id, user.state, req);
        }).catch(err => console.log(err));
    }
};


chooseAction = (id, userState, req) => {
    if (req.body.entry[0].messaging[0].message) {
        if (req.body.entry[0].messaging[0].message.quick_reply) {
            chooseActionBasedOnQuickReply(id, req.body.entry[0].messaging[0].message.quick_reply.payload);
        } else {
            chooseActionBasedOnText(id, userState, req.body.entry[0].messaging[0].message.text);
        }
    }
    if (req.body.entry[0].messaging[0].postback) {
        chooseActionBasedOnButton(id, userState, req.body.entry[0].messaging[0].postback.payload);
    }
};

chooseActionBasedOnQuickReply = (id, payload) => {
    switch (payload) {
        case constants.PAYLOAD.DETAILS:
            messagingService.sendGenericTemplate(id);
            userService.updateUser(id, constants.STATE.DETAILS).then(doc => console.log(doc)).catch(err => console.log(err));
            break;
        case constants.PAYLOAD.FIND:
            let buttons = [messaging.formQuickReply(constants.REPLY.FIND_BY_YEAR, constants.PAYLOAD.YEAR),
                messaging.formQuickReply(constants.REPLY.FIND_BY_AUTHOR, constants.PAYLOAD.AUTHOR)];
            messagingService.sendText(id, constants.REPLY.SELECT_SEARCH_TYPE_MSG, buttons);
            userService.updateUser(id, constants.STATE.SEARCH).then(doc => console.log(doc)).catch(err => console.log(err));
            break;
        case constants.PAYLOAD.YEAR:
            messagingService.sendText(id, constants.REPLY.ENTER_YEAR);
            userService.upd(id, 'searchByYear');
            break;
        case constants.PAYLOAD.AUTHOR:
            messagingService.sendText(id, constants.REPLY.ENTER_AUTHOR);
            userService.upd(id, 'searchByAuthor');
    }
};

chooseActionBasedOnText = (id, state, text) => {
    switch (state) {
        case 'start' :
            sendChooseActionMessage(id);
            break;
        case 'searchByYear':
            processFindByYearRequest(id, text);
            break;
        case 'searchByAuthor':
            processFindByAuthorRequest(id, text);
    }
};

chooseActionBasedOnButton = (id, state, payload) => {
    switch (payload.split('?')[0]) {
        case 'bookId':
            processGetBookDetailsRequest(id, payload.split('=')[1]);
            break;
        case 'find':
            let buttons = [messaging.formQuickReply(constants.REPLY.FIND_BY_YEAR, "year"), messaging.formQuickReply("Find by author", "author")];
            messagingService.sendText(id, constants.REPLY.SELECT_SEARCH_TYPE_MSG, buttons);
            userService.upd(id, 'search');
            break;
        case "details":
            messagingService.sendGenericTemplate(id);
            userService.updateUser(id, 'details').then(doc => console.log(doc)).catch(err => console.log(err));
            break;
        case 'year':
            messagingService.sendText(id, constants.REPLY.ENTER_YEAR);
            userService.upd(id, 'searchByYear');
    }
};

processFindByYearRequest = (id, text) => {
    let yearRegex = /^\d+$/;
    if (!yearRegex.test(text)) {
        messagingService.sendText(id, constants.REPLY.INCORRECT_INPUT);
    } else {
        sendBooksByYear(id, parseInt(text));
    }
};

sendBooksByYear = (id, year) => {
    if (year > new Date().getFullYear()) {
        messagingService.sendText(id, constants.REPLY.INVALID_YEAR);
    } else {
        bookService.findBooksByYear(year).then(books => {
            if (books.length === 0) {
                messagingService.sendText(id, (constants.REPLY.ANY_BOOK_WRITTEN_BY).concat(text));
            }
            books.forEach(book => {
                let response = formBookDetailsMessage(book);
                messagingService.sendText(id, response);
            })
        }).catch(err => console.log(err));
        userService.upd(id, 'start');
    }
};

processFindByAuthorRequest = (id, text) => {
    bookService.findBooksByAuthor(text).then(books => {
        if (books.length === 0) {
            messagingService.sendText(constants.REPLY.ANY_BOOK_WRITTEN_IN.concat(text));
        }
        books.forEach(book => {
            let response = formBookDetailsMessage(book);
            messagingService.sendText(id, response);
        })
    }).catch(err => console.log(err));
    userService.updateUser(id, 'start').then(doc => console.log('user upd', doc)).catch(err => console.log(err));
};

sendChooseActionMessage = (id) => {
    let buttons = [
        messaging.formQuickReply("Find book", "find"), messaging.formQuickReply("Book details", "details")
    ];
    messagingService.sendText(id, 'Choose action', buttons);
    userService.updateUser(id, 'choose').then(doc => console.log('user upd', doc)).catch(err => console.log(err));
};

processGetBookDetailsRequest = (id, bookId) => {
    if (bookId === undefined) {
        console.log('Wrong request');
    } else {
        bookService.findBookById(bookId).then(book => {
            let response = "Name: ".concat(book.name, "\nAuthor: ", book.authors, "\nYear: ", book.year);
            messagingService.sendText(id, response);
        }).catch(err => {
            console.log(err);
        })
    }
    userService.updateUser(id, 'start').then(doc => console.log(doc)).catch(err => console.log(err));
};

formBookDetailsMessage = (book) => {
    return "Name: ".concat(book.name, "\nAuthor: ", book.authors, "\nYear: ", book.year);
};
