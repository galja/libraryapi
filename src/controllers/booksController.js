let mongoose = require('mongoose');
let Book = require('../models/book');
let bookService = require('../service/bookService');

exports.getAllBooks = function (req, res) {
    let pageNumber = req.query.page;
    let size = 2;

    if (pageNumber < 0 || pageNumber === 0) {
        res.send('Invalid page');
    }

    bookService.findAllBooks(pageNumber, size).then((books) => {
        res.send(books);
    }).catch((err) => console.log(err));
};

exports.createBook = function (req) {
    let newBook = req.body;
    bookService.insertBook(newBook)
        .then(doc => {
            res.send(newBook);
        }).catch(err => {
        console.error(err)
    });
};

exports.getBookById = function (req, res) {
    const id = req.params.id;
    bookService.findBookById(id).then(book => {
        res.send(book);
    }).catch(err => {
        console.log(err);
    })
};

exports.updateBook = function (req, res) {

    bookService.updateBook({_id: req.params.id}, req.body.name).then(book => {
        res.send(book);
    }).catch(err => {
        console.log(err);
    })
};

exports.deleteBook = (req, res) => {

    bookService.deleteBook(req.params.id).then(res => {
        if (res.deletedCount === 0) {
            res.send('Can`t find this book');
        } else {
            res.send('Book removed');
        }
    }).catch(err => {
        console.log(err);
    });
};

exports.getBooksByYear = (req, res) => {
    bookService.findBooksByYear(req.params.year).then(books => {
        res.send(books);
    }).catch(err => console.log(err));
};

exports.getBooksByAuthor = (req, res) => {
    bookService.findBooksByAuthor(req.params.author).then(books => {
        req.send(books);
    }).catch(err => console.log(err));
};