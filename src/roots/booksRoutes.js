module.exports = function (server) {


    let routeBooks = '/books';
    let routeBooksById = '/books/:id';
    let routeBooksByYear = '/books/years/:year';
    let routeBooksByAuthor = '/books/authors/:author';

    let routeWebhook = '/webhook';

    let books = require('../controllers/booksController');
    let webhook = require('../controllers/webhook');

    server.get(routeBooks, books.getAllBooks);
    server.post(routeBooks, books.createBook);

    server.get(routeBooksById, books.getBookById);
    server.put(routeBooksById, books.updateBook);
    server.del(routeBooksById, books.deleteBook);

    server.get(routeBooksByYear, books.getBooksByYear);
    server.get(routeBooksByAuthor, books.getBooksByAuthor);

    server.post(routeWebhook, webhook.processRequest);
    server.get(routeWebhook, webhook.verifyWebhook);
};