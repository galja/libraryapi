let Book = require('../models/book');

exports.findAllBooks = async (pageNumber, size) => {
    let query = {};
    query.skip = size * (pageNumber - 1);
    query.limit = size;

    return await Book.find().limit(query.limit).skip(query.skip);
};

exports.insertBook = async (book) => {
    return await book.save();
};

exports.findBookById = async (id) => {
    return await Book.findOne({_id: id});
};

exports.updateBook = async (id, newName) => {
    return await Book.findByIdAndUpdate({id: id}, {$set: {name: newName}}, {new: true});
};

exports.deleteBook = async (id) => {
    return await Book.remove({_id: id});
};

exports.findBooksByYear = async (year) => {
    return await Book.find({year: year});
};

exports.findBooksByAuthor = async (author) => {
    return await Book.find({authors: {$all: author}});
};