let User = require('../models/user');

exports.findUser = async (id) => {
    return await User.findOne({id: id});
};

exports.insertUser = async (id, state)=>{
    return await User.create({id:id, state:state});
};

exports.updateUser = async (id, state)=>{
    return await User.updateOne({id: id}, {$set: {state: state}}, {new: true});
};

exports.upd = (id, state)=>{
    User.updateOne({id: id}, {$set: {state: state}}).then(doc => {
        console.log(doc)
    }).catch(err => {
        console.error(err)
    });
};