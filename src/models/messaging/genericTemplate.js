exports.genericTemplateMessage = (userId, elements) => {
    return {
        recipient: {
            id: userId
        },
        message: {
            attachment: {
                type: 'template',
                payload: {
                    template_type: "generic",
                    elements: elements
                }
            }
        }
    };
};