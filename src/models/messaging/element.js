exports.element = (imageUrl, title, buttons) => {
    return {
        image_url: imageUrl,
        title: title,
        buttons: [buttons]
    }
};