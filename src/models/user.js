let mongoose = require('mongoose');

let userSchema = new mongoose.Schema({
    id: String,
    state: String
});

module.exports = mongoose.model('User', userSchema);
