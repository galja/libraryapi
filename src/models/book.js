let mongoose = require('mongoose');

let bookSchema = new mongoose.Schema({
    authors: [],
    year: Number,
    name: String,
    pages: Number,
    language: String
});

module.exports = mongoose.model('Book', bookSchema);
