exports.formGetStartedButton = () => {
    return {
        get_started: {
            payload: "GET_STARTED_PAYLOAD"
        }
    };
};

exports.messageData = (id, message) => {
    return {
        messaging_type: "RESPONSE",
        recipient: {
            id: id
        },
        message: {
            text: message
        }
    }
};

exports.quickReplyMessageData = (userId, text, buttons) => {
    return {
        messaging_type: "RESPONSE",
        recipient: {
            id: userId
        },
        message: {
            text: text,
            quick_replies: buttons
        }
    };
};

exports.formQuickReply = (title, payload) => {
    return {
        content_type: 'text',
        title: title,
        payload: payload,
    }
};

exports.formButton = (title, payload) => {
    return {
        type: "postback",
        title: title,
        payload: payload
    }
};







// let q = require('../models/lkseji');
// let quick_replies = [
//     q.formQuickReply("Find by year", "year"),q.formQuickReply("Find by author", "author")
// ];
// messagingService.sendText(id, 'hi', quick_replies);


